import React from "react";
import axios from 'axios';
import './App.css';


class NouvellePharmacie extends React.Component {
    
    constructor(props) {
        super(props);
        this.state = {
        nom:'',
        quartier:'',
        ville:'',
        garde: '',
        };
        this.handleChange = this.handleChange.bind(this);
     
    this.handleSubmit = this.handleSubmit.bind(this);
    //this.handleInputChange = this.handleInputChange.bind(this);
    }


   ///


    // handleChange(event) {    this.setState({value: event.target.value});  }
    // handleSubmit(event) {
    //    alert('Le nom a été soumis : ' + this.state.value);
    //    event.preventDefault();
    //  }
    
     render() {
       return (
           <form onSubmit={this.handleSubmit}>
           <p><label id="nom">
             Nom :
             <input type="text" name="nom" value={this.state.nom} onChange={this.handleChange} />        </label></p>
            <p><label id="quartier">
             Quartier :
             <input type="text" name="quartier" value={this.state.quartier} onChange={this.handleChange} />        </label></p>
             <p><label id="ville">
             Ville :
             <input type="text" name="ville" value={this.state.ville} onChange={this.handleChange} />        </label></p>
             <p><label id="garde">
             Garde :
             <input type="text" name="garde" value={this.state.garde} onChange={this.handleChange} />        </label></p>
           <input type="submit" value="Envoyer" />
         </form>
       );
    }

    handleChange(event){
        this.setState({
            [event.target.name] :event.target.value
        });
        // console.log(this.state.nom)
    }
    
    handleSubmit(event){
       event.preventDefault();
            axios.post('http://127.0.0.1:8000/pharma', {nom:this.state.nom,quartier:this.state.quartier,ville:this.state.ville,garde:this.state.garde}
                )
                .then(function (reponse) {
                    //On traite la suite une fois la réponse obtenue 
                    console.log(reponse);
                })
                .catch(function (erreur) {
                    //On traite ici les erreurs éventuellement survenues
                    console.log(erreur);
                });
            
    }

  
}

export default NouvellePharmacie;
