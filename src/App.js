import React, { useState } from 'react';
import Menu from './Menu';
import PharmaList from './PharmaList';
import Garde from './Garde';
import NouvellePharmacie from './NouvellePharmacie';

import './App.css';

function App() {

  const [page, setPage] = useState('page1');
  const [garde, setGarde] = useState('*');

  const menuClick = id => setPage(id);
  const gardeClick = day => setGarde(day.target.value);

  return (
    <div className="App">
      <header className="App-header">
        <nav>
          <Menu title="Liste des pharmacies" id="page1" onClick={menuClick} />
          <Menu title="Ajouter une pharmacie" id="page2" onClick={menuClick} />
          <Garde onChange={gardeClick} />
        </nav>
      </header>
      <main>

        {page === "page1" &&
          <>
            {garde !== '*' &&
              <h2>
                Les pharmacies de garde le
                <span className="App-link"> {garde}</span>
              </h2>
            }

            {garde === '*' &&
              <h2>
                <span className="App-link">Toutes </span>
                les pharmacies
              </h2>
            }

            <PharmaList filter={garde} />
          </>
        }

        {page === "page2" &&
          
          <>

          <p>Ajoutez ici une nouvelle pharmacie</p>

{/* ici on recupere NouvellePharmacie.js */}
          <NouvellePharmacie/>

          </>
        }

      </main>
    </div>
  )
}

export default App;
